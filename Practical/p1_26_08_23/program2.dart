/*
	            *
	        * * *
	    * * * * *
	* * * * * * *
*/

import 'dart:io';

void main(){

	int n = 4;
	
	for(int i=1; i<=n; i++){
		for(int j=1; j<=(2*n-2*i); j++){
			stdout.write("  ");
		}
		for(int k=1; k<=2*i-1; k++){
			stdout.write("* ");
		}
		print("");
	}
}
