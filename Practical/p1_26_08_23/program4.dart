/*
	    *
	  * *
	* * *
	  * *
	    *
*/	

import 'dart:io';

void main(){

	int n = 5;
	
	for(int i=1; i<=n; i++){
		int j=0,sp=0;

		if(i<=n/2+1){
			j = i;
			sp = n~/2-i+1;
		}else{
			j = n-i+1;
			sp = i-n~/2-1;
		}

		for(int k=1; k<=sp; k++){
			stdout.write("  ");
		}
		
		for(int l=1; l<=j; l++){
			stdout.write("* ");
		}
		print("");
	}
}
