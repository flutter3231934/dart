/*
	                * *
	            * * * *
	        * * * * * *
	    * * * * * * * *
	* * * * * * * * * *
	    * * * * * * * *
	        * * * * * *
	            * * * *
	                * *
*/	

import 'dart:io';

void main(){

	int n = 9;
	int j=0,sp=0;
	
	for(int i=1; i<=n; i++){

		if(i<=n~/2+1){
			j = i*2;
			sp = n-2*i+1;
		}else{
			j = j-2;
			sp = sp+2;
		}

		for(int k=1; k<=sp; k++){
			stdout.write("  ");
		}
		
		for(int l=1; l<=j; l++){
			stdout.write("* ");
		}
		print("");
	}
}
