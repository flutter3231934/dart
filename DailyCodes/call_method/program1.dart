
class Parent{

	Parent(){
		print("In parent constructor");
		this();
	}

	call(){
		print("In call method");
	}
}

void main(){

	Parent obj = new Parent();
	obj();
}
