
//factory constructor

class Demo{

	Demo._private(){
		print("In private constructor");
	}

	factory Demo(){
		print("In factory constructor");
		return Demo._private();
	}
}
