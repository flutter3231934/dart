
abstract class Developer{
	
	factory Developer(String devType){
		
		if(devType == 'Backend')
			return Backend();
		else if(devType == 'Frontend')
			return Frontend();
		else if(devType == 'Mobile')
			return Mobile();
		else
			return Other();
	}

	void devLang();
}

class Backend implements Developer{
	
	void devLang(){
		print("SpringBoot/NodeJS");
	}
}

class Frontend implements Developer{
	
	void devLang(){
		print("ReactJS/AngularJS");
	}
}

class Mobile implements Developer{
	
	void devLang(){
		print("Flutter/Android/Kotlin");
	}
}

class Other implements Developer{
	
	void devLang(){
		print("Testing/DevOps/Support");
	}
}

void main(){

	Developer obj1 = new Developer("Backend");
	Developer obj2 = new Developer("Frontend");
	Developer obj3 = new Developer("Mobile");
	Developer obj4 = new Developer("Web");

	obj1.devLang();
	obj2.devLang();
	obj3.devLang();
	obj4.devLang();
}
