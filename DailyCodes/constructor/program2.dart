
//Named constructor

class Demo{
	
	Demo(){
		print("Normal Constructor");
	}

	Demo.one(){
		print("Named constructor1");
	}
	
	Demo.two(){
		print("Named constructor2");
	}
}

void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo.one();
	Demo obj3 = new Demo.two();
}
