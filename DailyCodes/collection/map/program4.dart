
import 'dart:collection';

void main(){

	var player = LinkedHashMap();

	//way 1
	player[18] = 'Virat';
	print(player);

	//way 2
	player.addAll({45:'Rohit'});
	print(player);

	//way 3
	player.addEntries({7:'MSD',1:'KLRahul'}.entries);
	print(player);
}
