
import 'dart:collection';

void main(){

	var compData = Queue();
	//var compData = ListQueue();	//default implementation

	compData.add('Amazon');
	compData.add('Microsft');
	compData.add('Google');

	print(compData);
	print(compData.runtimeType);

	compData.addFirst('Apple');
	compData.addLast('NVIDIA');
	
	print(compData);

	compData.removeFirst();
	compData.removeLast();
	
	print(compData);
}
