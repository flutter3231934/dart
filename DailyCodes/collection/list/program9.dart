
//add elements

void main(){

	var progLang = List.empty(growable:true);

	progLang.add('Cpp');
	progLang.add('Java');
	progLang.add('Python');
	progLang.add('Java');

	print(progLang);

	var lang = ['ReactJS','Springboot','Flutter'];

	progLang.addAll(lang);
	print(progLang);

	progLang.insert(3,'Dart');	
	print(progLang);

	progLang.insertAll(3,['Go','Swift']);
	print(progLang);
	
	progLang.replaceRange(3,7,['Dart','Swift']);
	print(progLang);

	//remove elements

	progLang.remove('ReactJS');
	print(progLang);
	
	progLang.removeAt(4);
	print(progLang);
	
	progLang.removeLast();
	print(progLang);
	
	progLang.clear();
	print(progLang);
}
