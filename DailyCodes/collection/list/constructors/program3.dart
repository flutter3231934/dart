
//unmodifiable()

void main(){

	List player1 = ['Virat', 'ABD', 'Yuvraj', 'Warner'];
	List player2 = List.unmodifiable(player1);

	print(player1);
	print(player2);

	player1[3] = 'Gayle';
	
	print(player1);
	print(player2);
	
	player2[2] = 'Chahal';	//error

	print(player1);
	print(player2);
}
