
//empty()

void main(){
	
	List player1 = List.empty();	//empty length
	List player2 = List.empty(growable:true);	//growable length

	//player1.add("Virat");		//error
	//player2[0] = "de Villers";	//error

	player2.add("Virat");
	player2.add("de Villiers");

	print(player2);
	
	player2[0] = 'Virat Kohli';

	print(player2);
}
