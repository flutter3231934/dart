//named arguments to function

void fun({String? name, double? sal}){
	print("In fun");
	print(name);
	print(sal);
}

void main(){
	
	print("Start main");
	fun(sal:55.5,name:"Jaydeep");
	print("End main");
}
