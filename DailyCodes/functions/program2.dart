

void fun(String name, [double sal = 10.8]){
	print("In fun");
	print(name);
	print(sal);
}

void main(){
	
	print("Start main");
	fun("Jaydeep");
	fun("Jaydeep",55.5);
	print("End main");
}
