
class Parent{

	Parent(){
		print("Parent constructor");
	}
}

class Child extends Parent{

	Child(){
		super();	//error: superclass has no 'call' method
		print("Child constructor");
	}
}

void main(){

	Child obj = new Child();
}
