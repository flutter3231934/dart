
class Parent{

	int? x;
	String? str;

	Parent(this.x, this.str);

	void printData(){
		print(x);
		print(str);
	}
}

class Child extends Parent{

	int? y;
	String? str1;

	Child(this.y, this.str1, this.x, this.str);

	void printData1(){
		print(y);
		print(str1);
	}
}

void main(){

	Child obj = new Child(10,"Kanha",20,"BMC");
	//error
}
