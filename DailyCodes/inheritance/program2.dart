
class UNIX{

	int fYear = 1969;
	String fName = 'Dennis Ritchie';

	void unixDisp(){
		print("In unixDisp method");
	}
}

class Linux extends UNIX{
	
}

void main(){

	Linux obj = new Linux();

	print(obj.fYear);
	print(obj.fName);
	obj.unixDisp();
}
