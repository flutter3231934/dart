
class Parent{

	int x = 10;
	String str1 = "Madhur";
	
	get getX => x;
	get getStr1 => str1;
}

class Child extends Parent{

	int y = 20;
	String str2 = "Vinay";
	
	get getY => y;
	get getStr2 => str2;
}

void main(){

	Child obj = new Child();

	print(obj.getX);	//10
	print(obj.getStr1);	//Madhur
	print(obj.getY);	//20
	print(obj.getStr2);	//Vinay
}
