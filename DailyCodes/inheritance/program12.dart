
class Parent{

	Parent(){
		print("Parent constructor");
	}

	call(){
		print("In method call");
	}
}

class Child extends Parent{

	Child(){
		super();
		print("Child constructor");
	}
}

void main(){

	Child obj = new Child();
	obj();
}
