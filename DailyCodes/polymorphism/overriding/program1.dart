
class Parent{

	void career(){

		print("Engineering");
	}

	void marry(){

		print("Megan Fox");
	}
}

class Child extends Parent{

	void career(){

		print("Engineering");
	}

	void marry(){

		print("Jenna Ortega");
	}
}

void main(){

	Child obj = new Child();
	
	obj.career();
	obj.marry();
}
