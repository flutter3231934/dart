
abstract class Parent{

	void property(){

		print("bunglows,cars,flats,gold");
	}

	//abstract methods
	void career();
	void marry();
}

class Child extends Parent{

	void career(){

		print("Ethical Hacker");
	}

	void marry(){

		print("Jenna Ortega");
	}
}

void main(){

	Parent obj = new Child();
	
	obj.property();
	obj.career();
	obj.marry();
}
