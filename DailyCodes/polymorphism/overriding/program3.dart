
class Linux{

	String? founder;
	int? year;

	Linux(this.founder, this.year);

	void ui(){

		print("command line");
	}

	void sysCalls(){

		print("UNIX based");
	}
}

class Ubuntu extends Linux{

	String? founder;

	Ubuntu(this.founder,String name, int year):super(name,year);

	void ui(){

		print("GUI");
	}
}

void main(){

	Linux obj = new Ubuntu("Mark Richard Shuttleworth","Linus Torvalds",1991);

	obj.ui();
	obj.sysCalls();
}
