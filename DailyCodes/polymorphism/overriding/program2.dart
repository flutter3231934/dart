
class Parent{

	void career(){

		print("Engineering");
	}

	void marry(){

		print("Megan Fox");
	}
}

class Child extends Parent{

	void career(){

		print("Engineering");
	}

	void marry(){

		print("Jenna Ortega");
	}
	
	void proff(){

		print("Soft Engg.");
	}
}

void main(){

	Parent obj = new Parent();
	
	obj.career();
	obj.marry();
	obj.proff();		//error: method is not defined in parent
}
