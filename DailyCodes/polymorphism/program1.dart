
class Demo{

	int x = 10;
	int y = 20;

	void disp(int x){
		this.x = x;
		print(x);
	}

	void disp(int x, int y){	//error: overloading is not supported in dart
		this.x = x;
		this.y = y;
		print(x);
		print(y);
	}
}

void main(){
	
	Demo obj = new Demo();
	obj.disp(50);
	obj.disp(60,70);
}
