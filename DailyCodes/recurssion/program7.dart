
int fact(int num){

	if(num == 1)
		return 1;
	
	return num * fact(--num);
}

void main(){

	print(fact(4));
}
