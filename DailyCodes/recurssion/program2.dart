
void fun(int num){

	if(num > 10)
		return;

	print(num);
	
	fun(++num);	
}

void main(){

	fun(1);
}
