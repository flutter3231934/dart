
import 'program2.dart';

void main(){

	Singleton obj1 = new Singleton();
	Singleton obj2 = new Singleton();
	Singleton obj3 = new Singleton();

	print(obj1.hashCode);
	print(obj2.hashCode);
	print(obj3.hashCode);
}
