
//singleton design pattern using factory constructor

class Singleton{

	static Singleton obj = new Singleton._private();

	Singleton._private(){
		print("constructor");
	}

	factory Singleton(){
		return obj;
	}
}
