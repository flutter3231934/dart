
//singleton design pattern without factory constructor

class Singleton{

	static Singleton obj = new Singleton._private();

	Singleton._private(){
		print("contructor");
	}

	static Singleton getInstance(){
	
		return obj;
	}
}
